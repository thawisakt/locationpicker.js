/**
 * locationpicker.js
 * Written by Kim Grytøyr, 2015. Based on locationpicker.js
 * by Dmitry Berezovsky, Logicify (http://logicify.com/)
 */

"use strict";

(function ( $ ) {

    function GMap(_domElement, config) {
        var _map = new google.maps.Map(_domElement, config);
        var _marker = new google.maps.Marker({
            position: new google.maps.LatLng(config.settings.location.latitude, config.settings.location.longitude),
            map: _map,
            title: config.settings.markerTitle,
            draggable: config.draggable
        });
        return {
            map: _map,
            marker: _marker,
            circle: null,
            location: _marker.position,
            radius: config.radius,
            domElement: _domElement,
            settings: config.settings,
            geodecoder: new google.maps.Geocoder()
        }
    }

    var MapUtils = {
        drawCircle: function(map, location, radius, options) {
            if (map.circle != null) {
                map.circle.setMap(null);
            }
            if (radius > 0) {
                radius *= 1;
                options.map = map.map;
                options.radius = radius;
                options.center = location;
                map.circle = new google.maps.Circle(options);
                if (options.editable) {
                    map.circle.setEditable(true);
                    google.maps.event.addListener(map.circle, "radius_changed", function(event) {
                        setRadius(map, Math.round(map.circle.radius));
                    });
                    google.maps.event.addListener(map.circle, "center_changed", function(event) {
                        MapUtils.setPosition(map, map.circle.center, function(context) {
                            var currentLocation = MapUtils.locationFromLatLng(map.location);
                            context.settings.afterChange.apply(map.domElement, [currentLocation, context.radius]);
                        });
                    });
                }
                return map.circle;
            }
            return null;
        },

        locationFromLatLng: function(lnlg) {
            return {
                latitude: lnlg.lat(),
                longitude: lnlg.lng()
            }
        },

        setPosition: function(map, location, callback) {
            map.location = location;
            map.marker.setPosition(location);
            map.map.panTo(location);
            this.drawCircle(map, location, map.radius, map.settings.circleOptions);
            if (callback) {
                callback.call(this, map);
            }
        }
    }

    function isPluginApplied(el) {
        return getContextForElement(el) != undefined;
    }

    function getContextForElement(el) {
        return $(el).data("locationpicker");
    }

    function autosize(map) {
        google.maps.event.trigger(map.map, 'resize');
        setTimeout(function() {
            map.map.setCenter(map.marker.position);
        }, 300);
    }

    function setRadius(map, radius) {
        if (radius < map.settings.minRadius) {
            radius = map.settings.minRadius;
        }
        if (radius > map.settings.maxRadius) {
            radius = map.settings.maxRadius;
        }

        map.radius = radius;
        MapUtils.setPosition(map, map.marker.position, function(context) {
            var currentLocation = MapUtils.locationFromLatLng(map.location);
            context.settings.afterChange.apply(map.domElement, [currentLocation, context.radius]);
        });
    }

    function addressSearch(map, address, callback) {
        if (address.length > 2) {
            map.geodecoder.geocode({'address': address}, function(results, status) {
                if(status == google.maps.GeocoderStatus.OK  && results && results.length) {
                    MapUtils.setPosition(map, results[0].geometry.location, function(context) {
                        context.settings.afterChange.apply(map.domElement,
                            [MapUtils.locationFromLatLng(context.location), context.radius]);
                        if (map.settings.zoomLevelAfterAddressResult) {
                            map.map.setZoom(map.settings.zoomLevelAfterAddressResult);
                        }
                    });
                } else {
                    if (callback) {
                      callback.call(this, { noResults: true });
                    }
                }
            });
        }
    }

    $.fn.locationpicker = function( config, params, callback ) {
        if (typeof config == 'string') {
            var _targetDomElement = this.get(0);
            if (!isPluginApplied(_targetDomElement)) {
                console.log("ERROR: locationpicker.js not loaded on element...");
                return;
            }
            var map = getContextForElement(_targetDomElement);
            switch (config) {
                case "address":
                    if (params == undefined) {
                        return;
                    } else {
                        addressSearch(map, params, callback);
                    }
                    break;
                case "location":
                    if (params == undefined) {
                        var location = MapUtils.locationFromLatLng(map.location);
                        return location;
                    } else {
                        MapUtils.setPosition(map, new google.maps.LatLng(params.latitude, params.longitude), function(context) {
                            var currentLocation = MapUtils.locationFromLatLng(map.location);
                            context.settings.afterChange.apply(map.domElement, [currentLocation, context.radius]);
                        });
                    }
                    break;
                case "radius":
                    if (params == undefined) {
                        return map.radius;
                    } else {
                        if (params > 0) {
                            setRadius(map, params);
                        }
                    }
                    break;
                case "autosize":
                    autosize(map);
                    return this;
                case "destroy":
                    if (isPluginApplied(_targetDomElement)) {
                        $(_targetDomElement).removeData("locationpicker");
                        $(_targetDomElement).removeAttr("locationpicker");
                        $(_targetDomElement).html('');
                        return true;
                    } else {
                        return false;
                    }
            }
            return null;
        }
        var _domElement = $(this);

        if (isPluginApplied(_domElement)) { // Only if already initialized
            return;
        }
        config = $.extend({}, $.fn.locationpicker.defaults, config );

        if (config.minRadius > config.maxRadius || config.maxRadius < config.minRadius) {
            throw "Invalid radius/minRadius/maxRadius values";
            return;
        }

        if (config.radius > config.maxRadius) {
            console.log("WARNING: radius is bigger than maxRadius. Setting radius=maxRadius.");
            config.radius = config.maxRadius;
        }
        if (config.radius < config.minRadius) {
            console.log("WARNING: radius is smaller than minRadius. Setting radius=minRadius.");
            config.radius = config.minRadius;
        }

        return this.each(function() {
            if (isPluginApplied(this)){
                console.log("WARNING: Plugin already loaded on element..");
                return;  
            } 
            var map = new GMap(this, {
                zoom: config.zoom,
                center: new google.maps.LatLng(config.location.latitude, config.location.longitude),
                mapTypeId: config.mapType,
                mapTypeControl: config.mapTypeControl,
                zoomControl: config.zoomControl,
                panControl: config.panControl,
                streetViewControl: config.streetViewControl,
                radius: config.radius,
                settings: config,
                draggable: config.draggable
            });
            _domElement.data("locationpicker", map);

            if (config.clickable) {
                google.maps.event.addListener(map.map, "click", function(event) {
                    MapUtils.setPosition(map, event.latLng, function(context) {
                        var currentLocation = MapUtils.locationFromLatLng(event.latLng);
                        context.settings.afterChange.apply(map.domElement, [currentLocation, context.radius]);
                    });
                });
            }
            google.maps.event.addListener(map.marker, "dragend", function(event) {
                MapUtils.setPosition(map, map.marker.position, function(context) {
                    var currentLocation = MapUtils.locationFromLatLng(map.location);
                    context.settings.afterChange.apply(map.domElement, [currentLocation, context.radius]);
                });
            });
            MapUtils.setPosition(map, map.marker.position, function(context) {
                var currentLocation = MapUtils.locationFromLatLng(map.location);
                context.settings.afterLoad.apply(map.domElement, [currentLocation, context.radius]);
            });
        });
    }

    $.fn.locationpicker.defaults = {
        location: {
            latitude: 69.675366,
            longitude: 18.975428
        },
        minRadius: 15,
        maxRadius: 300,
        radius: 150,
        zoom: 15,
        zoomLevelAfterAddressResult: 15,
        draggable: true,
        clickable: true,
        mapType: google.maps.MapTypeId.HYBRID,
        mapTypeControl: true,
        panControl: true,
        zoomControl: true,
        streetViewControl: false,
        circleOptions: {
            strokeColor: "#0000FF",
            strokeOpacity: 0.45,
            strokeWeight: 2,
            fillColor: "#0000FF",
            fillOpacity: 0.10,
            editable: true
        },
        markerTitle: "Dra markøren til uteposten",
        afterLoad: function(currentLocation, radius) {},
        afterChange: function(currentLocation, radius) {},
    }
}( jQuery ));