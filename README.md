locationpicker.js is a simple-to-use jQuery plugin that allows you to select a location on a map and receive the coordinates using a callback function. It also supports searching for a location based on an address.

## Examples
Here are two live examples of how locationpicker.js works.

**Modal window** (with address search)
http://dev.kimgrytoyr.com/locationpicker.js/example/

**Fullscreen**
http://dev.kimgrytoyr.com/locationpicker.js/example/index_fullscreen.html

## Requirements
locationpicker.js requires jQuery and the Google Maps API to work.

## Usage
These are the scripts you need to include in your HTML file:

```html
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="//maps.google.com/maps/api/js?sensor=false&libraries=places"></script>
<script src="js/locationpicker.js"></script>
```
In addition, you'll need to insert a container that will hold the actual map from Google Maps:

```html
<div id="myMap"></div>
```
The map container can be styled using CSS.

The last thing you need to do is to load the locationpicker.js plugin:

```html
<script>
	$('#myMap').locationpicker({ options });
</script>
```

## Options
You can configure locationpicker.js with a few options. Here are the defaults:

```js
location: {
	latitude: 69.675366,
	longitude: 18.975428
},
minRadius: 15,
maxRadius: 300,
radius: 150,
zoom: 15,
zoomLevelAfterAddressResult: 15,
markerTitle: "Dra markøren til uteposten",
draggable: true,
clickable: true,
mapType: google.maps.MapTypeId.NORMAL,
mapTypeControl: true,
zoomControl: true,
panControl: true,
streetViewControl: false,
circleOptions: {
    strokeColor: "#0000FF",
    strokeOpacity: 0.45,
    strokeWeight: 2,
    fillColor: "#0000FF",
    fillOpacity: 0.10,
    editable: true
},
afterLoad: function(currentLocation, radius) {},
afterChange: function(currentLocation, radius) {},
```

**location { latitude, longitude }** (default: 69.675366, 18.975428)
The coordinates that will be loaded when the map is first displayed.

**minRadius** (default: 15)
The minimum radius (in meters) that the user can set.

**maxRadius** (default: 300)
The maximum radius (in meters) that the user can set.

**radius** (default: 150)
The radius (in meters) that will be used when the map is first displayed.

**zoom** (default: 15)
The zoom level that will be used when the map is first displayed.

**zoomLevelAfterAddressResult** (default: same as `zoom`)
After a successful address search, the map's zoom level will be reset to the value of this parameter. To disable this behavior, set the value to `false`.

**draggable** (default: true)
If set to false, dragging the map to pan will not be possible.

**clickable** (default: true)
If set to true, the user will be able to change location only by clicking on the map. This will also change the location when double clicking to zoom in, so you can consider setting it to false.

**mapType** (default: google.maps.MapTypeId.HYBRID)
The map type to use. Valid options:

* google.maps.MapTypeId.ROADMAP (normal map)
* google.maps.MapTypeId.SATELITTE (satellite map) 
* google.maps.MapTypeId.HYBRID (normal + satellite)
* google.maps.MapTypeId.TERRAIN (shows terrain info)

**mapTypeControl** (default: true)
If set to false, the user will not be able to change the mapType.

**zoomControl** (default: true)
If set to false, the zoom controls will be hidden.

**panControl** (default: true)
If set to false, the pan controls will be hidden.

**streetViewControl** (default: false)
If set to true, street view control will be shown.

**circleOptions { .. }**
Options:
* strokeColor: The color of the stroke around the radius circle.
* strokeOpacity: The opacity of the stroke around the radius circle
* strokeWeight: The weight/width of the stroke around the radius circle
* fillColor: The color of the background fill of the radius circle
* fillOpacity: The opacity of the background fill of the radius circle
* editable: true|false (true will allow the user to change radius size by dragging the edges of the radius circle)

**markerTitle** (default: Dra markøren)
The title that will be displayed when the mouse is hovered over the marker.

**afterLoad:** function(currentLocation, radius)
A callback function that is called after the initial load of the map. `currentLocation` is an object with the properties `latitude` and `longitude`. `radius` contains the current radius.


**afterChange:** function(currentLocation, radius)
A callback function that is called every time the location or radius changes. `currentLocation` is an object with the properties `latitude` and `longitude`. `radius` contains the current radius.

## Address search
The plugins also supports searching for a location by providing the address. Here's an example of how to do that:

```js
$('#myMap').locationpicker('address', 'Karl Johans gate 1, Oslo, Norway', function(e) {
    if (e.noResults) {
        // No results found, alert the user.
    }
});
```

If Google Maps' geocoder is able to find a location, the first location returned from Google will be selected in the map. If not, the object `e` will have a property called `noResults` with the value `true`.

__Note:__ If a location is found, the map's zoom level will be reset to the value provided in the config parameter `zoomLevelAfterAddressResult` if it's not set to `false`.
